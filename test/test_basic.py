import narrator_AI
import os
import re

def test_import():
    """Package testing stub."""
    assert narrator_AI.TEST_VAR == 1


def test_narration():
    txt_path = os.path.join(os.path.dirname(__file__), 'text.txt')
    narrator = narrator_AI.Narration(txt_path)
    path = os.path.join(os.path.dirname(__file__),
                        f'sentiment_narration.wav')
    narrator.text2speech(path)
    assert os.path.exists(path)

def test_add_audio():
    audio_path = os.path.join(os.path.dirname(__file__), 'test.wav')
    video_path = os.path.join(os.path.dirname(__file__), 'news_0_1_cut.mp4')
    path = os.path.join(os.path.dirname(__file__), 'test_audio_anim.mp4')
    narrator_AI.add_audio(video_path , audio_path, path)
    assert os.path.exists(path)

def test_frame_animation():
    # df_path = os.path.join(os.path.dirname(__file__), 'test.csv')
    # print(os.listdir(os.path.dirname(__file__)))
    # frames = [f for f in os.listdir(os.path.dirname(__file__)) if re.match("pogodynka_" + ".*\.jpg$", f)]
    # print(frames)
    #frames = [os.path.join(os.path.dirname(__file__), 'pogodynka_a.jpg'),os.path.join(os.path.dirname(__file__), 'pogodynka_f_w.jpg')]
    #path_af = os.path.join(os.path.dirname(__file__), 'shorttest.wav')
    print(os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), "test", "test.csv"))
    narrator_AI.frame_anim(test = 1)
