# Copyright (c) 2016-2018 Braintech Sp. z o.o. [Ltd.] <http://www.braintech.pl>
# All rights reserved.
from setuptools import setup, find_packages

import versioneer


INSTALL_REQUIREMENTS = {
    'setup': [
    ],
    'install': [
        'numpy==1.17.3',
        'requests==2.27.1',
        'beautifulsoup4==4.10.0',
        'pygame==2.1.2',
        'Pillow==8.4.0',
        'matplotlib==3.3.4',
        'scipy==1.4.1',
        'regex==2021.4.4',
        "nltk==3.4.5",
        "pandas==1.3.5"
    ],
}

if __name__ == '__main__':
    setup(
        name='narrator_AI',
        version=versioneer.get_version(),
        cmdclass=versioneer.get_cmdclass(),
        description='Narrator AI. '
                    'The program creates an animated video based on '
                    'a website address with news. '
                    'The video includes voice narration of the message '
                    'and matching images and music.',
        zip_safe=False,
        author='Agata Kulesza, Anna Bożuk, Weronika Plichta',
        author_email='ad.kulesza2@student.uw.edu.pl, a.bozuk@student.uw.edu.pl, w.plichta@student.uw.edu.pl',
        license='Other/Proprietary License',
        classifiers=[
            'Development Status :: 3 - Alpha',
            'Natural Language :: English',
            'Topic :: Scientific/Engineering',
            'Intended Audience :: Developers',
            'Programming Language :: Python :: 3',
            'Programming Language :: Python :: 3.7',
        ],
        keywords='narrator AI',
        packages=find_packages(include=['narrator_AI']),
        include_package_data=True,
        setup_requires=INSTALL_REQUIREMENTS['setup'],
        install_requires=INSTALL_REQUIREMENTS['install'],
        extras_require=INSTALL_REQUIREMENTS,
        entry_points={
            'console_scripts': [],
        },
        ext_modules=[],
    )
