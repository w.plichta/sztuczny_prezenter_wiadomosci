from os.path import dirname

from . import _version
from .add_audio import add_audio
from .frame_animation import *
import os
from .text_to_speech import ESpeak
from .text_sentiment_analysis import sentiment_analysis
from .narration import Narration


__version__ = _version.get_versions()['version']

TEST_VAR = 1
TEXT_TO_SPEECH_PATH = os.path.join(dirname(__file__), "text_to_speech.sh")
REPO_PATH = dirname(dirname(__file__))


if __name__ == "__main__":
    pass
