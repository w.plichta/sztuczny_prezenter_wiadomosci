import contextlib
import os
import random
import re
import time
import wave

import PIL.Image
import pandas as pd
import pygame
import sys


class sprites(pygame.sprite.Sprite):

    def __init__(self, pattern, sentiment=1, test=0):
        """

        :param pattern: pattern of a filename that contains desired image
        :param sentiment: number that corresponds to sentiment of news
        """
        pygame.sprite.Sprite.__init__(self)
        # searching for image files in current folder which names match the given pattern

        if test == 0:
            self.frames = [os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), "test", f) for f in
                           os.listdir(os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), "test"))
                           if
                           re.match(pattern, f)]
        else:
            path = "/builds/sztuczny-prezenter-wiadomosci/sztuczny_prezenter_wiadomosci/"
            self.frames = [os.path.join(path, "test", f) for f in os.listdir(os.path.join(path, "test")) if
                           re.match(pattern, f)]
        print(self.frames)
        self.sprites = [pygame.image.load(f) for f in self.frames]
        self.current = 0
        self.image = self.sprites[self.current]
        self.rect = self.image.get_rect()

    def update(self):
        """
        Function updates current frame that is displayed
        """
        self.current += 1
        if int(self.current) >= len(self.sprites):
            self.current = 0

        random.shuffle(self.sprites)  # making random frame order
        self.image = self.sprites[int(self.current)]


def convert(sentiment, width, height, fr, input_frames, output_animation, video_length):
    """
    Function saves created frame animation to mp4 file and cuts its length so that it last desired amount of time
    :param sentiment: number that corresponds to sentiment of news
    :param width: width size of a video that is equal to width of every single frame
    :param height: height size of a video that is equal to height of every single frame
    :param fr: frame rate of a video, matches frame rate of created frame animation
    :param input_frames: names of files with created frames
    :param output_animation: name of mp4 video with created animation
    :param video_length: cuts the mp4 video to the desired duration
    :return:
    """
    cut_output = output_animation[:-4] + "_" + str(sentiment) + "_cut.mp4"  # name of cut mp4 video
    # added automatic overwriting files
    command1 = f"ffmpeg -y -r {fr} -f image2 -s {str(width)}x{str(height)} -i {input_frames} -vcodec libx264 -vf \"pad=ceil(iw/2)*2:ceil(ih/2)*2 \" -crf {fr}  -pix_fmt yuv420p {output_animation}"

    command2 = f"ffmpeg -y -ss 00:00:00 -to {video_length}  -i {output_animation} -c copy {cut_output}"
    os.system(command1)
    os.system(command2)


def frame_anim(test=0):
    pygame.init()

    if test == 0:
        data_file = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), "test", "test.csv")
    else:
        path = "/builds/sztuczny-prezenter-wiadomosci/sztuczny_prezenter_wiadomosci/"
        data_file = os.path.join(path, "test", "test.csv")

    print(data_file)

    df = pd.read_csv(data_file, header=[0])
    video_count = 0  # counter if more than one video will be created
    # reading an audio file

    for i in range(df.shape[0]):

        print(df["audio_fname"][i])
        audio_f = df["audio_fname"][i]
        # size of chosen frames
        # pattern that matches desired images
        filename_pattern = "pogodynka_" + ".*\.jpg$"

        sent = df["sentiment"][i]  # sentiment of the news text

        if test == 0:
            img_file = (
                os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), "test", "pogodynka_a.jpg"))
            af = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), "test", audio_f)
        else:
            img_file = os.path.join(path, "test", "pogodynka_a.jpg")
            af = os.path.join(path, "test", audio_f)
        image = PIL.Image.open(img_file)  # example image frame
        width, height = image.size
        screen_width = width
        screen_height = height

        screen = pygame.display.set_mode((screen_width, screen_height), flags=pygame.HIDDEN)

        anim = pygame.sprite.Group()

        anim.add(sprites(filename_pattern, sent, test=test))

        words_per_minute = 175  # set while creating narration,can be modified, option to read the parameter from csv data file can be added

        with contextlib.closing(wave.open(af, 'r')) as f:
            frames = f.getnframes()
            rate = f.getframerate()
            duration = frames / float(
                rate)  # duration of an audio file; option to read the parameter from csv data file can be added
        fps = 2 * words_per_minute / 60  # frames per second, depending on narration speed

        frame_count = 0

        while (pygame.time.get_ticks() / 1000) <= duration * 2:

            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    sys.exit()

            anim.draw(screen)
            frame_count += 1
            filename = "screen_" + str(sent) + "_%05d.png" % (frame_count)
            if test == 0:
                curr_path = os.path.join(os.path.dirname(__file__), filename)
            else:
                curr_path = os.path.join(path, "narrator_AI", filename)
            pygame.image.save(screen, curr_path)

            anim.update()
            pygame.display.flip()

            pygame.time.Clock().tick(fps)

        pygame.quit()
        print("pygame quit")
        fname = "screen_" + str(sent) + "_%05d.png"
        video_name = "news_%0d.mp4" % (video_count)  # loop needed
        print(video_name)
        trim_time = str(time.strftime('%H:%M:%S', time.gmtime(duration)))
        if test == 0:
            path_fname = os.path.join(os.path.dirname(__file__), fname)
            path_video = os.path.join(os.path.dirname(__file__), video_name)
        else:
            path_fname = os.path.join(path, "narrator_AI", fname)
            path_video = os.path.join(path, "narrator_AI", video_name)
        convert(sent, screen_width, screen_height, fps, path_fname, path_video, trim_time)

        cut_output = video_name[:-4] + "_" + str(sent) + "_cut.mp4"
        df.loc[i, "animation_fname"] = cut_output
        df.to_csv(data_file)
        video_count += 1

        # deleting single frames created in pygame
        for f in os.listdir('.'):
            if re.match("screen_" + str(sent) + ".*\.png$", f):
                if test == 1:
                    os.remove(os.path.join(path, "narrator_AI", f))
                else:
                    os.remove(os.path.join(os.path.dirname(__file__), f))


if __name__ == '__main__':
    frame_anim()
