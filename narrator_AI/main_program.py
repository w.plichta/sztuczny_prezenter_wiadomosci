# -*- coding: utf-8 -*-
"""
Created on Wed Apr 27 11:47:56 2022

@author: weron
"""
import text_processing
import text_sentiment_analysis
import photo_downloader

'''First version of function downloading pictures based on text sentiment analysis'''


def get_pictures(filename):
    file1 = f'./neuro/news_nr_1.txt'

    text_processing.dshortext(filename)
    most_common_1, compound_1 = text_sentiment_analysis.sentiment_analysis(file1)

    if compound_1 > 0:  # tylko dla jednego, żeby zobaczyć czy tak nie będzie wygodniej zwracać wartosci
        comp = "Positive"
    else:
        comp = "Negative"

    keyword1_1 = most_common_1[0][0]
    keyword1_2 = most_common_1[1][0]

    photo_downloader.photo_downloader(f'https://unsplash.com/s/photos/{keyword1_1}-{keyword1_2}', '{keyword1_1}')
    return comp


get_pictures(f'./url_list')
