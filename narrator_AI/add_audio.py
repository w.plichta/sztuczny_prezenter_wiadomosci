# -*- coding: utf-8 -*-
"""
Created on Wed Apr 20 12:47:00 2022

@author: annab
"""

import os
import pandas as pd


def add_audio(inputvideo, audiofile, outputvideo):
    """
    Function merges animation with narration audio.
    :param inputvideo: name of an mp4 file without sound
    :param audiofile: name of an audio file with narration
    :param outputvideo: name of a video with added sound

    """
    command = f"ffmpeg -i {inputvideo} -i {audiofile} -c:v copy -map 0:v:0 -map 1:a:0 -c:a aac -b:a 192k {outputvideo}"
    os.system(command)


if __name__ == "__main__":
    data_file = "../test/test.csv"  # name of a file with data needed for animation
    df = pd.read_csv(data_file, header=[0])
    for i in range(df.shape[0]):
        animation_with_audio = "audio_news_%0d.mp4" % (i)
        add_audio(df["animation_fname"][i], df["audio_fname"][i], animation_with_audio)
        df.loc[i, "audio_anim"] = animation_with_audio
        df.to_csv(data_file)
