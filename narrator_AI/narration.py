import pandas as pd
import os

from narrator_AI import sentiment_analysis, ESpeak
import narrator_AI


class Narration:
    """
    Create narration

    Args:
        text_path - path to news text

    Methods:
        text2speech - generate voice narration
    """

    def __init__(self, text_path):
        self._text_path = text_path
        self._common_words, self._compound = sentiment_analysis(text_path)
        self._common_words = [w[0] for w in self._common_words]

    def text2speech(self, output_path, gender="f"):
        """
        Generate voice narration modulated by
        the sentiment of the text.

        Args:
            output_path - audio output path
            gender - gender of narrator
                "f" - for female
                "m" - for male
        """
        # voice, narrator frames, speed
        narration_types = {
            (True, "f"): ["karen-happy", "Margaret_Thatcher", 72],
            (False, "f"): ["karen-sad", "angry_karen", 65],
            (True, "m"): ["bob-happy", "overcaffinated_jerry", 75],
            (False, "m"): ["bob-sad", "bald_kamil", 65],
        }

        narration_prop = narration_types[self._compound > 0, gender]
        narration = ESpeak(narration_prop[0])
        audio_len = narration.generate_text_to_speech(self._text_path, output_path)

        csv_path = output_path.replace(".wav", ".csv")
        self._get_csv(output_path, audio_len, narration_prop[2],
                      narration_prop[1], csv_path)

    def _get_csv(self, audio_path, audio_len, speed,
                 frame_dir, output_path):
        """
        Generates csv file with metadata

        Args:
            audio_path - path to narration file
            audio_len - length of narration in seconds
            speed - speed of narration voice
            frame_dir - name of directory with frames to animation
            output_path - csv output path
        """
        df_dict = {
            "text_path": [self._text_path],
            "audio_fname": [audio_path],
            "audio_length": [audio_len],
            "speed": [speed],
            "sentiment": [self._compound],
            "keywords": [str(self._common_words)],
            "frames_path": [os.path.join(narrator_AI.REPO_PATH, "narrator_frames", frame_dir)],
            "video_fname": [None]
        }
        df = pd.DataFrame.from_dict(df_dict)
        df.to_csv(output_path)
