# -*- coding: utf-8 -*-
"""
Created on Mon Mar 14 15:19:56 2022

@author: annab
"""

''' 
Function takes a name of txt file with a list of url addresses as an argument and save texts from the websites in separate files
'''

import requests
from bs4 import BeautifulSoup
import os
import re
import io


def dshortext(filename):
    '''
     version with short news from shortpedia
    '''
    with open(filename + '.txt') as f:
        lines = f.readlines()
    directory = f'./neuro/'

    if not os.path.exists(directory):
        os.makedirs(directory)

    for idx, url in enumerate(lines):

        page = requests.get(url.strip())
        soup = BeautifulSoup(page.content, 'html.parser')

        for br in soup.find_all("br"):
            br.replace_with("\n")

        shorttext = soup.find("meta", attrs={'name': 'description'})
        # print(abstract["content"] if abstract else "No meta title given")
        title = (str(soup.title))
        title = re.sub('<.*?>', '', title)
        title = re.sub('( \-).*$', '', title)

        # with io.open(directory+title.split(' ')[0]+'.txt', "w", encoding="utf-8") as f:
        with io.open(directory + 'news_nr_' + str(idx + 1) + '.txt', "w", encoding="utf-8") as f:
            f.write(title + '\n')
            f.write(str(shorttext["content"]))


dshortext('url_list')
