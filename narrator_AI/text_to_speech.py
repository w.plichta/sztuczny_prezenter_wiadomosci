import os
import re
import scipy.io.wavfile
from scipy.ndimage import gaussian_filter1d
import narrator_AI

IGNORED_WORDS = ["for", "up", "out",
                 "off", "of", "down",
                 "a", "an"]


class ESpeak:
    """
    Manages Espeak
    Args:
        voice - name of voice e.g. karen-happy
    """
    def __init__(self, voice="mb-us1"):
        self._voice = voice

    def generate_text_to_speech(self, text_path, output_path):
        """
        Generates a text to speech .wav file
        Args:
            text_path - path to text file
            output_path - path to output .wav file
        """
        modify_text(text_path, self._voice)
        pho_path = output_path[:-4] + "_phonemes.pho"
        cmdline = f"sh {narrator_AI.TEXT_TO_SPEECH_PATH} {text_path} {output_path} {self._voice} {pho_path}"
        os.system(f"chmod +x {narrator_AI.TEXT_TO_SPEECH_PATH}")
        os.system(cmdline)
        audio_len = voice_norm(output_path, self._voice)
        return audio_len


def modify_text(text_fname, voice):
    """
    Modify text so the narrator's voice
    can be better understood.
    Args:
        text_fname - path to text file
        voice - name of voice
    """
    f = open(text_fname)
    txt = f.read()
    f.close()

    a = re.findall('[A-Z][a-z]*', txt)

    for word in a:
        txt = txt.replace(word, word.upper())

    break_in_word = 13
    if "sad" in voice:
        break_in_word -= 2

    txt_list = txt.split()
    for i in range(break_in_word, len(txt_list), break_in_word - 1):
        if txt_list[i][-1] == '.' or txt_list[i] in IGNORED_WORDS:
            continue
        txt_list.insert(i + 1, ',')
    txt_list.insert(0, ',')

    txt = ' '.join([str(elem) for elem in txt_list])

    f = open(text_fname, "w")
    f.write(txt)
    f.close()


def voice_norm(audio_fname, voice):
    """
    Flattens the narrator's voice audio.
    Args:
        audio_fname - path to generated
            narration aduio file

        voice - name of voice
    """
    fs, voice_sig = scipy.io.wavfile.read(audio_fname)
    audio_length = len(voice_sig)/fs
    flatten_ratio = 1.55
    if "sad" in voice:
        flatten_ratio += 0.35
    voice_sig = gaussian_filter1d(voice_sig, flatten_ratio)
    scipy.io.wavfile.write(audio_fname, fs, voice_sig)
    return audio_length
