# -*- coding: utf-8 -*-
"""
Created on Mon Mar 14 15:19:56 2022

@author: annab
"""

"funkcja bierze na wejscie link i zapisuje artykuł z danego linku do pliku txt"

import requests
from bs4 import BeautifulSoup
#import time
import os
import re
import io

purl1 = "https://neurosciencenews.com/myoelectric-signals-tetraplegia-20171/"
purl2 = "https://www.frontiersin.org/articles/10.3389/fnins.2022.831714/abstract"
purl3 = "https://www.frontiersin.org/articles/10.3389/fneur.2012.00054/full"


def dtext(url):
    directory = f'./neuro/'
    if not os.path.exists(directory):
         os.makedirs(directory)
    page_url = url
    page = requests.get(page_url)
    soup = BeautifulSoup(page.content, 'html.parser')
    for br in soup.find_all("br"):
                br.replace_with("\n")
    all_p = soup.find_all('p')


    #print(page.content) #kod zaczynający się od 2 zwykle oznacza sukces, a kod zaczynający się od 4 lub 5 wskazuje na błąd.





    with io.open(directory+'example.txt', "w", encoding="utf-8") as f:
        f.write(re.sub('<.*?>', '', str(soup.title)+'\n'))
        for data  in all_p[:2]: #wczytuje 2 pierwsze paragrafy

            clean_text = re.sub('<.*?>', '', str(data.get_text()))
            f.write(str(clean_text)+'\n')




def dabstract(url):
    "wersja z wczytywaniem abstraktu"

    directory = f'./neuro/'

    if not os.path.exists(directory):
         os.makedirs(directory)

    page_url = url
    page = requests.get(page_url)
    soup = BeautifulSoup(page.content, 'html.parser')

    for br in soup.find_all("br"):
                br.replace_with("\n")

    abstract = soup.find("meta", attrs={'name': 'citation_abstract'})

    title = (str(soup.title))
    title = title.split('|')[1][1:-1]
    
    with io.open(directory+title.split(' ')[0]+'.txt', "w", encoding="utf-8") as f:
        f.write(title+'\n')
        f.write(str(abstract["content"]))



#dtext(purl)
dabstract(purl2)
dabstract(purl3)
    